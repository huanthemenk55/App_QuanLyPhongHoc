@IF EXIST "%~dp0\echo.exe" (
  "%~dp0\echo.exe"  off & "%~dp0\node_modules\@squarespace\server\build\distributions\local-developer\bin\run.bat" %*
) ELSE (
  @SETLOCAL
  @SET PATHEXT=%PATHEXT:;.JS;=;%
  echo  off & "%~dp0\node_modules\@squarespace\server\build\distributions\local-developer\bin\run.bat" %*
)