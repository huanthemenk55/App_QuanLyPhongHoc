import React,{Component} from "react";
import {
    AppRegistry, View ,Text,StyleSheet,TouchableOpacity,Dimensions,Image,ImageBackground
} from "react-native";
import { StackNavigator,TabNavigator } from 'react-navigation';
import back from "../images/back.png";
import bg from "../images/bg5.jpg";
const {width,height} = Dimensions.get('window');
export class DetailsNhaBay extends Component{
    render(){
        const {params} = this.props.navigation.state;
        const{
            textStyle,boxtitleStyle,texttitleStyle,wrapper
        }=styles
        return(
            <ImageBackground source={bg} style={{flex:1,padding:10}}>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate("NhaBay")}}
                                  style={{height:height/15,width:width, flexDirection:"row"}}>
                    <Image source={back} style={{height:20,width:20}} />
                    <Text style={{fontSize:15,color:"#2196f3"}}> Trở lại </Text>
                </TouchableOpacity>
                <View style={boxtitleStyle}>
                    <Text style={texttitleStyle}>Phòng {params.product.Room.toString().toUpperCase()}</Text>
                </View>
                <View style={wrapper}>
                    <Text style={textStyle}> Lớp đang học tại phòng: {params.product.Class.toString().toUpperCase()}</Text>
                    <Text style={textStyle}> {params.product.Detail}</Text>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    boxtitleStyle:{
        alignItems:"center",justifyContent:"center",
    },
    texttitleStyle:{
        color:"red",fontSize:25
    },
    textStyle:{
        fontSize:15,color:"black",
    },
    wrapper:{
        marginTop:20,
        justifyContent:"center",
    }
});
