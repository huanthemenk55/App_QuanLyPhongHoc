import React, {Component} from "react";
import {
    AppRegistry,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    Image,
    TextInput,
    ImageBackground,
    TouchableWithoutFeedback,
    Keyboard,Alert
} from "react-native";
import {StackNavigator, TabNavigator} from 'react-navigation';

const {width, height} = Dimensions.get('window');
import bg from "../images/bg5.jpg";
import quanli from "../images/login3.png";
import GetSignIn from "../api/GetSignIn";
export class SignIn extends Component {
    constructor(props){
        super(props);
        this.state={
            un:"",
            pa:"",
            kq:"",
            token:"..."
        }
    }
    onFail() {
        Alert.alert(
            'Thông báo',
            'Sai Username hoặc Password',
            [
                { text: 'OK' }
            ],
            { cancelable: false }
        );
    }
    LOGIN(){
        const {un,pa}=this.state;
        GetSignIn(un,pa)
            .then((responseJson)=>{
                this.setState({
                    kq:responseJson["token"]
                });
                if (this.state.kq === "OK") {
                    this.props.onOpen();
                }
                else return this.onFail();
            })

    }
    render() {
        const {
            inputStyle, bigButton, buttonText, wrapper,textStyle,
            dongtext,imageStyle,thongBao,textThongBao
        } = styles;
        return (
                <ImageBackground source={bg} style={{flex:1}}>
                    <View style={dongtext}>
                        <Image source={quanli} style={imageStyle} />
                        <Text style={textStyle}>Đăng Nhập</Text>
                    </View>
                    <View style={wrapper}>
                        <TextInput
                            style={inputStyle}
                            placeholder="Nhập Username"
                            underlineColorAndroid="transparent"
                            onChangeText={(un) => this.setState({un})}
                            value={this.state.un}
                        />
                        <TextInput
                            style={inputStyle}
                            placeholder="Nhập Password"
                            underlineColorAndroid="transparent"
                            secureTextEntry
                            onChangeText={(pa) => this.setState({pa})}
                            value={this.state.pa}
                        />
                        <TouchableOpacity style={bigButton} onPress={()=>{this.LOGIN()}}>
                            <Text style={buttonText}>Đăng Nhập</Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        justifyContent: 'center', paddingLeft: 10,alignItems:"center"

    },
    inputStyle: {
        height: 50,
        width: width - 20,
        backgroundColor: '#fff',
        marginBottom: 10,
        borderWidth: 1,
        //borderBottomColor:"red",
        borderRadius: 20,
        paddingLeft: 30,
    },
    bigButton: {
        height: 50,
        borderRadius: 20,
        backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'center',
        width: width - 20,
    },
    buttonText: {
        fontFamily: 'Avenir',
        color: '#fff',
        fontWeight: '400'
    },
    textStyle:{
        fontSize:15,color:"black",
    },
    dongtext:{
        height:height/3.5,width:width,
        justifyContent: 'center',alignItems:"center",
    },
    imageStyle:{
        height:60,width:60
    },
    thongBao:{
        justifyContent:"center",alignItems:"center",
        width:width,height:50
    },
    textThongBao:{
        fontSize:12,color:"red",
    }
});

