import React, {Component} from "react";
import {
    AppRegistry,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    Image,
    TextInput,
    ImageBackground,
    TouchableWithoutFeedback,
    Keyboard,
    Alert
} from "react-native";
import {StackNavigator, TabNavigator} from 'react-navigation';

const {width, height} = Dimensions.get('window');
import ModalDropdown from 'react-native-modal-dropdown';
import bg from "../images/bg5.jpg"
import quanli from "../images/home.png"
import GetCapNhat from "../api/GetCapNhat";

export class CapNhat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ROOM: "",
            CLASS: "",
            STATE: "",
            NHA:"",
            result: ""
        }
    }
    onSuccess() {
        Alert.alert(
            'Thông báo',
            'Cập Nhật Thành Công',
            [
                { text: 'OK' }
            ],
            { cancelable: false }
        );
    }

    clickPlus() {
        const {ROOM, CLASS, STATE,NHA} = this.state;
        GetCapNhat(ROOM, CLASS, STATE, NHA)
            .then((rsjson) => {
                    this.setState({
                        result: rsjson["MaCode"],

                    });
                    if (this.state.result === 'Cập Nhật Thành Công') return this.onSuccess();
                }
            )

    }

    abc(idx, value) {
        this.setState({STATE: `${value[0]}`})
    }
    xyz(idx, value) {
        this.setState({ROOM: `${value}`})
    }
    xxx(idx, value) {
        this.setState({CLASS: `${value}`})
    }
    nha(idx, value) {
        this.setState({NHA: `${value[value.length-1]}`})
    }
    render() {
        const {
            inputStyle, bigButton, buttonText, wrapper,
            textStyle, dongtext, imageStyle, madiStyle, trong,trong1
        } = styles;
        return (
            <ImageBackground source={bg} style={{flex: 1}}>
                <View style={dongtext}>
                    <Image source={quanli} style={imageStyle}/>
                    <Text style={textStyle}>Quản Lý</Text>
                </View>
                <View style={wrapper}>
                    <ModalDropdown
                        style={inputStyle}
                        textStyle={madiStyle}
                        dropdownStyle={trong1}
                        defaultIndex={0}
                        defaultValue={'Khu Nhà'}
                        options={['Nhà 6','Nhà 7']}
                        rescrollEnabled={true}
                        onSelect={(idx, value) => this.nha(idx, value)}
                    />
                    <ModalDropdown
                        style={inputStyle}
                        textStyle={madiStyle}
                        dropdownStyle={trong}
                        defaultIndex={0}
                        defaultValue={'Phòng Cần Cập Nhật'}
                        options={   ['101', '102', '103',
                                    '201','202', '203',
                                    '301', '302', '303',
                                    '601','602','603']}
                        rescrollEnabled={true}
                        onSelect={(idx, value) => this.xyz(idx, value)}
                    />
                    <ModalDropdown
                        style={inputStyle}
                        textStyle={madiStyle}
                        dropdownStyle={trong}
                        defaultIndex={0}
                        defaultValue={'Lớp Học'}
                        options={[  'B1D48', 'B2D48', 'B3D48','B4D48', 'B5D48', 'B6D48','B7D48', 'B8D48', 'B9D48',
                                    'B1D47', 'B2D47', 'B3D47','B4D47', 'B5D47', 'B6D47','B7D47', 'B8D47', 'B9D47',
                                    'B1D49', 'B2D49', 'B3D49','B4D49', 'B5D49', 'B6D49','B7D49', 'B8D49', 'B9D49',]}
                        rescrollEnabled={true}
                        onSelect={(idx, value) => this.xxx(idx, value)}
                    />
                    <ModalDropdown
                        style={inputStyle}
                        textStyle={madiStyle}
                        dropdownStyle={trong}
                        defaultIndex={0}
                        defaultValue={'Trạng Thái Phòng'}
                        options={['1-Phòng Đang Học', '2-Phòng Chưa Học', '3-Phòng Trống']}
                        rescrollEnabled={true}
                        onSelect={(idx, value) => this.abc(idx, value)}
                    />
                    <TouchableOpacity style={bigButton} onPress={() => {
                        this.clickPlus()
                    }}>
                        <Text style={buttonText}>Cập Nhật</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={bigButton} onPress={() => this.props.onOpen()}>
                        <Text style={buttonText}>Đăng Xuất</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        paddingLeft: 10, alignItems: "center",
        justifyContent: "space-between"

    },
    inputStyle: {
        height: 50,
        width: width - 20,
        backgroundColor: '#fff',
        marginBottom: 10,
        borderWidth: 1,
        borderRadius: 20,
        paddingLeft: 30,
        justifyContent: "center",
    },
    madiStyle: {
        fontSize: 14,
        justifyContent: "center",

    },
    trong: {
        width: width - 50,
        height: 110,
        borderWidth: 1,
        borderColor:"black",
        borderRadius: 5,
        overflow: 'hidden',
    },
    trong1: {
        width: width - 50,
        height: 70,
        borderWidth: 1,
        borderColor:"black",
        borderRadius: 5,
        overflow: 'hidden',
    },
    bigButton: {
        height: 50,
        borderRadius: 20,
        backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
        width: width - 20,
    },
    buttonText: {
        fontFamily: 'Avenir',
        color: '#fff',
        fontWeight: '400'
    },
    textStyle: {
        color: "black",
    },
    dongtext: {
        height: height / 6.5, width: width,
        justifyContent: 'center', alignItems: "center",
    },
    imageStyle: {
        height: 60, width: 60
    }
});
