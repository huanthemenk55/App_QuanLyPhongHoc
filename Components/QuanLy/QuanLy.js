import React, {Component} from "react";
import {
    AppRegistry,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    Image,
    TextInput,
    ImageBackground,
    TouchableWithoutFeedback,
    Keyboard
} from "react-native";
import {StackNavigator, TabNavigator} from 'react-navigation';

const {width, height} = Dimensions.get('window');
import bg from "../images/bg5.jpg"
import quanli from "../images/home.png"
import {SignIn} from "./SignIn";
import {CapNhat} from "./CapNhat";

export class QuanLy extends Component {
    constructor(progs){
        super(progs);
        this.state={
            user:true,
        }
    }
    ManHinhChuaLogin(){
        this.setState({
            user:false
        })
    }
    ManHinhDaLogin(){
        this.setState({
            user:true
        })
    }
    render() {
        const {
            inputStyle, bigButton, buttonText, wrapper, textStyle, dongtext, imageStyle
        } = styles;
        const mainQuanLy=this.state.user ? <SignIn onOpen={this.ManHinhChuaLogin.bind(this)}/>
                                            : <CapNhat onOpen={this.ManHinhDaLogin.bind(this)}/>
        return (
            <View style={{flex:1}}>
            {mainQuanLy}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        paddingLeft: 10, alignItems: "center",
        justifyContent:"space-between"

    },
    inputStyle: {
        height: 50,
        width: width - 20,
        backgroundColor: '#fff',
        marginBottom: 10,
        borderWidth: 1,
        //borderBottomColor:"red",
        borderRadius: 20,
        paddingLeft: 30,
    },
    bigButton: {
        height: 50,
        borderRadius: 20,
        backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom:10,
        width: width - 20,
    },
    buttonText: {
        fontFamily: 'Avenir',
        color: '#fff',
        fontWeight: '400'
    },
    textStyle: {
        fontSize: 15, color: "black",
    },
    dongtext: {
        height: height / 4, width: width,
        justifyContent: 'center', alignItems: "center",
    },
    imageStyle: {
        height: 60, width: 60
    }
});

