import React, {Component} from "react";
import {
    AppRegistry, View, Text, Image, Dimensions,TouchableWithoutFeedback,Keyboard
} from "react-native";
import {StackNavigator, TabNavigator,DrawerNavigator} from 'react-navigation';
import {NhaSau} from "./Nha6/Nha6";
import {NhaBay} from "./Nha7/Nha7";
import {QuanLy} from "./QuanLy/QuanLy";
import {SignIn} from "./QuanLy/SignIn";
import {Header} from "./Header";
import {Menu} from "./Menu";
import home2 from "./images/hometab.png";
import user from "./images/user.png"
const {width, height} = Dimensions.get('window');
export const MyApp = TabNavigator({
        Nha6: {
            screen: NhaSau,
            navigationOptions: {
                tabBarLabel: "Nhà 6",
                tabBarIcon: ({tintColor}) => (
                    <Image source={home2} style={{height: 20, width: 20, tintColor: tintColor}}/>
                )
            }
        },
        Nha7: {
            screen: NhaBay,
            navigationOptions: {
                tabBarLabel: "Nhà 7",
                tabBarIcon: ({tintColor}) => (
                    <Image source={home2} style={{height: 20, width: 20, tintColor: tintColor}}/>
                )
            }
        },
        QuanLy: {
            screen: QuanLy,
            navigationOptions: {
                tabBarLabel: "Quản Lý",
                tabBarIcon: ({tintColor}) => (
                    <Image source={user} style={{height: 20, width: 20, tintColor: tintColor}}/>
                )
            }
        },
    },
    {
        tabBarOptions: {
            showIcon: true,
            activeTintColor: '#2196f3',
            inactiveTintColor : '#2E272B',
            upperCaseLabel: false,
            labelStyle: {
                fontSize: 11, margin: 3,
            },
            tabStyle: {
                height: height / 14,
            },
            style:{
                backgroundColor:"white",
            }
        },
        tabBarPosition: "bottom",
    }
)

export class App extends Component {
    OpenDrawer(nameDrawer) {
        const hihi = this.props.navigation.navigate;
        hihi(nameDrawer);
    }
    render() {
        console.disableYellowBox = true;
        return (
            <TouchableWithoutFeedback onPress={() => {
                Keyboard.dismiss();
            }}>
            <View style={{flex: 1}}>
                <Header/>
                <MyApp/>
            </View>
            </TouchableWithoutFeedback>
        );
    }
}
