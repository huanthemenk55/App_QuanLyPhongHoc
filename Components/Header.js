import React, {Component} from "react";
import {
    AppRegistry, View, Text, StyleSheet, TouchableOpacity, Dimensions, Image
} from "react-native";
import {StackNavigator, TabNavigator} from 'react-navigation';

const {width, height} = Dimensions.get('window');
import iconMenu from "./images/sky1.png";
import iconLogo from "./images/ic_logo.png";
export class Header extends Component {
    render() {
        const {
            imageStyle, textStyle, wrapper
        } = styles;
        return (
            <View style={wrapper}>
                <Image source={iconMenu} style={imageStyle}/>
                <Text style={textStyle}>Quản Lý Phòng Học </Text>
                <Image source={iconLogo} style={{width: 30, height: 30,margin:5}}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        width: width, height: height / 13, flexDirection: "row", backgroundColor: "blue",
        justifyContent: 'space-between'
    },

    imageStyle: {
        width: 35, height: 35,margin:5
    },
    textStyle: {
        fontSize: 16, color: "white",margin:6
    }
})
