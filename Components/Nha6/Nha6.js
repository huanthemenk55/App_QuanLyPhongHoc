import React, {Component} from "react";
import {
    AppRegistry, View, Text, StyleSheet, TouchableOpacity, FlatList,
    Image, ImageBackground,ScrollView,Dimensions,Keyboard,RefreshControl
} from "react-native";

import {StackNavigator, TabNavigator,NavigationActions} from 'react-navigation';
import {DetailsNhaSau} from "../Details/DetailsNhaSau";
import GetDataNha6 from "../api/GetDataNha6";
import iconHome from "../images/iconhome.png";
import bg from "../images/bg5.jpg";
const {width, height} = Dimensions.get('window');

export class ManHinhNhaSau extends Component {
    constructor(props){
        super(props);
        this.state={
            ROOM:"",
            result:"",
            lop:"",
            listProducts:"",
            refreshing:false,
        };
        this.arr = [];
    }
    gotoDetails(product){
        this.props.navigation.navigate('Details', {product});
    }
    componentDidMount() {
        GetDataNha6()
            .then(arrProduct => {
                this.arr = arrProduct;
                this.setState({ listProducts: this.arr });
            })

    }
    Init(progs){
        switch (progs){
            case '1':{
                return(
                    <View style={styles.boht}>
                        <View style={styles.tt1}>
                        </View>
                        <Text style={styles.styleText}>Phòng Đang Học</Text>
                    </View>
                )
            }
            case '2':{
                return(
                <View style={styles.boht}>
                    <View style={styles.tt2}>
                    </View>
                    <Text style={styles.styleText}>Phòng Chưa Học</Text>
                </View>
                )
            }
            case '3':{
                return(
                <View style={styles.boht}>
                    <View style={styles.tt3}>
                    </View>
                    <Text style={styles.styleText}>Phòng Trống</Text>
                </View>
                )
            }
        }
    }
    render() {
        const {
            tt1,tt2,tt3, boht,dong
        } = styles;
        return (

            <ImageBackground source={bg} style={styles.wrapper}>
                <FlatList
                    contentContainerStyle={dong}
                    refreshing={this.state.refreshing}
                    data={this.state.listProducts}
                    renderItem={({item}) => (
                    <TouchableOpacity style={styles.cot}
                                      onPress={() => this.gotoDetails(item)
                                      }>
                        <Image source={iconHome} style={styles.styleImage}/>
                        <Text style={styles.styleText}> Phòng:{item.Room.toString().toUpperCase()}</Text>
                        <Text style={styles.styleText}> Lớp:{item.Class.toString().toUpperCase()}</Text>
                        {this.Init(item.State)}
                    </TouchableOpacity>
                    )}
                    onRefresh={() => {
                                this.setState({ refreshing: true });
                                GetDataNha6()
                                    .then(arrProduct => {
                                        this.arr = arrProduct
                                        this.setState({
                                            listProducts: this.arr,
                                            refreshing: false
                                        });
                                    })

                            }}
                />
            </ImageBackground>
        );
    }
}

export const NhaSau = StackNavigator({
        NhaSau: {
            screen: ManHinhNhaSau,

        },
        Details: {
            screen: DetailsNhaSau,
        }
    },
    {
        mode: 'modal',
        headerMode: 'none',
    }
)

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    styleText: {
        fontSize: 10,
    },
    styleImage: {
        height: 50, width: 50
    },
    dong: {
        //justifyContent: "space-around",
        //flex:1,
        flexDirection: "row",flexWrap:"wrap",
},
    cot: {
        width:width/4,height:height/5, borderColor: "blue", borderWidth: 1, borderRadius: 30,margin:width/26,
        marginBottom:20,
        alignItems: "center", justifyContent: "center", backgroundColor: "#f1f1f1",
    },
    tt1: {
        backgroundColor: "red",
        borderRadius: 10,
        width:8,height:8
    },
    tt2: {
        backgroundColor: "#1beb09",
        borderRadius: 10,
        width:8,height:8
    },
    tt3: {
        backgroundColor: "#0951eb",
        borderRadius: 10,
        width:8,height:8
    },
    boht: {
        flexDirection: "row"
    }
});



