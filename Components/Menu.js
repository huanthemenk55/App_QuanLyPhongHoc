import React,{Component} from "react";
import {
    AppRegistry, View ,Text,TouchableOpacity,StyleSheet,Image,Dimensions
} from "react-native";


import { StackNavigator,TabNavigator,DrawerNavigator,NavigationActions  } from 'react-navigation';
import profile from "./images/profile.png";
const {width, height} = Dimensions.get('window');
import {SignIn} from "./QuanLy/SignIn";

export class Menu extends  Component{
    constructor(progs){
        super(progs);
        this.state=
            {
                user:true
            }
    }
    MoveAuthecation(name){
        this.props.navigation.navigate(name);
    }
    ManHinhChuaLogin(){
        this.setState({
            user:false
        });
    }
    ManHinhDaLogin(){
        this.setSatte({
            user:true
        })
    }
    render(){
        const {
            imageStyle,container,boxStyle,boxText,username,boxSignInStyle,boxTextSignIn,wrapper
        }=styles
        const ChuaSignIn=(
            <View style={{flex:1}}>
                <TouchableOpacity onOpen={this.ManHinhChuaLogin.bind(this)}
                                  style={boxStyle}
                                  onPress={()=> this.props.navigation.navigate("SignIn")}
                >
                    <Text style={boxText}>Sign In</Text>
                </TouchableOpacity>
            </View>
        );
        const DaSignIn=(
            <View style={wrapper}>
                <Text style={username}>HuanTheMen</Text>
                <View>
                    <TouchableOpacity style={boxSignInStyle} onPress={this.ManHinhChuaLogin.bind(this)}>
                        <Text style={boxTextSignIn}>Sign out</Text>
                    </TouchableOpacity>
                </View>
                <View />
            </View>
        )
        const mainMenu= this.state.user ? ChuaSignIn :DaSignIn;
        return(
            <View style={container}>
                <Image source={profile} style={imageStyle} />
                {mainMenu}
            </View>
        );
    }
}
const styles=StyleSheet.create({
        container:{
            flex:1,backgroundColor:"#34B089",
            alignItems:"center",
        },
        imageStyle:{
            width:120,height:120,borderRadius:60,marginVertical:30
        },
        boxStyle:{
            height: 50,
            backgroundColor: '#fff',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 5,
            paddingHorizontal: 70
        },
        boxText:{
            color: '#34B089',
            fontFamily: 'Avenir',
            fontSize: 20
        },
        username:{
            fontSize:15,
            color:"white"
        },
        boxTextSignIn:{
            fontSize:15,
            color:"#34B089"
        },
        boxSignInStyle:{
            height: 50,
            backgroundColor: '#fff',
            borderRadius: 20,
            width: width*0.7,
            marginBottom: 10,
            justifyContent: 'center',
            paddingLeft: 5,
        },
        wrapper:
            {
                flex:1,
                justifyContent: 'space-between',
                alignItems: 'center'
            }

    }
)
