const GetCapNhat = (r,c,s,n) => (
    fetch("http://psasky48.com/Update.php",
        {
            method:"POST",
            headers:{
                "Accept":"application/json",
                "Content-Type":"application/json"
            },
            body: JSON.stringify({
                "ROOM":r,
                "CLASS":c,
                "STATE":s,
                "NHA":n,
            })
        })
        .then((rs)=>rs.json())
);

export default GetCapNhat;
