const GetSignIn = (u,p) => (
    fetch("http://psasky48.com/SignIn.php",{
        method:"POST",
        headers:{
            "Accept":"application/json",
            "Content-Type":"application/json",
        },
        body: JSON.stringify({
            "USERNAME":u,
            "PASSWORD":p,
        })
    })
        .then((response)=>response.json())
);

export default GetSignIn;
